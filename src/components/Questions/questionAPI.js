export function fetchQuestions(questionURL){
    return fetch(questionURL).then(response =>response.json())
}