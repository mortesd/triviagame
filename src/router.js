import VueRouter from 'vue-router'
import PlayScreen from "@/components/PlayScreen";
import ResultScreen from "@/components/ResultScreen";
import StartingScreen from "@/components/StartingScreen";

const routes=[
    {
        path:"/",
        component: StartingScreen
    },
    {
        path:"/home",
        component: StartingScreen
    },
    {
        path:"/play",
        component: PlayScreen
    },
    {
        path:"/result",
        component: ResultScreen
    }
]


const router= new VueRouter({routes})

export default router